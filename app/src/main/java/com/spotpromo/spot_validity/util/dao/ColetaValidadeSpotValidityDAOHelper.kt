package com.spotpromo.spot_validity.util.dao

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.Produto
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.DAOHelper
import com.spotpromo.spot_validity.util.UtilidadesValidity
import java.util.ArrayList

class ColetaValidadeSpotValidityDAOHelper @Throws(Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {
    @Throws(Exception::class)
    fun insert(coletaSpotValidity: ValidadeSpotValidity): Int {

        if(coletaSpotValidity.id!! > 0)
            deletarValidade(coletaSpotValidity.id!!)

        val sbQuery = StringBuilder()
        sbQuery.append("TB_Coleta_Validade")

        val values = ContentValues()
        values.put("codPesquisa", coletaSpotValidity.codPesquisa)
        values.put("codCategoria", coletaSpotValidity.codCategoria)
        values.put("codMarca", coletaSpotValidity.codMarca)
        values.put("codSku", coletaSpotValidity.codSku)
        values.put("quantidade", coletaSpotValidity.quantidade)
        values.put("lote", coletaSpotValidity.lote)
        values.put("data_validade", coletaSpotValidity.data_validade)
        values.put("observacoes", coletaSpotValidity.observacoes)
        values.put("flRetorno", coletaSpotValidity.flRetorno)
        values.put("flPresente", coletaSpotValidity.flPresente)
        values.put("codMotivo", coletaSpotValidity.codMotivo)

        return this.db.insert(sbQuery.toString(), null, values).toInt()
    }

    fun deletarValidade(id: Int) {
        val tabelas = arrayOf(
            "TB_Coleta_Validade_Fotos WHERE idColetaValidade IN (?) ",
            "TB_Coleta_Validade WHERE id IN (?) "
        )

        for (i in tabelas.indices) {
            //Monta Query
            val query = String.format("DELETE FROM %s", tabelas[i])
            this.executaNoQuery(query, id)
        }
    }

    fun insertFoto(foto: String, idColeta: Int) {
        val sbQuery = StringBuilder()
        sbQuery.append("INSERT INTO TB_Coleta_Validade_Fotos (idColetaValidade, foto, flFotoEnviada) VALUES(?,?,?)")

        this.executaNoQuery(
            sbQuery.toString(),
            idColeta,
            foto,
            0
        )

    }

    fun selectValidadeEnvio(codPesquisa: Int) : ArrayList<ValidadeSpotValidity> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           p.*  , tp.desCategoria, tp.desMarca, tp.desSku                                               " +
                    "   FROM TB_Coleta_Validade p inner join TB_Produto tp on tp.codCategoria = p.codCategoria" +
                    " and   tp.codMarca = p.codMarca and tp.codSku = p.codSku   " +
                    " WHERE codPesquisa in (?)                                                               " +
                    "   ORDER BY desMarca                                                                    "
        )

        cursor = this.executaQuery(sbQuery.toString(), codPesquisa)

        val lista = ArrayList<ValidadeSpotValidity>()
        if (cursor.moveToFirst())
            do {
                val validadeSpotValidity = ValidadeSpotValidity(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getInt(cursor.getColumnIndex("codPesquisa")),
                    cursor.getInt(cursor.getColumnIndex("codCategoria")),
                    cursor.getInt(cursor.getColumnIndex("codMarca")),
                    cursor.getInt(cursor.getColumnIndex("codSku")),
                    cursor.getInt(cursor.getColumnIndex("quantidade")),
                    cursor.getString(cursor.getColumnIndex("lote")),
                    cursor.getString(cursor.getColumnIndex("data_validade")),
                    cursor.getString(cursor.getColumnIndex("observacoes")),
                    cursor.getInt(cursor.getColumnIndex("flRetorno")),
                    cursor.getInt(cursor.getColumnIndex("flPresente")),
                    cursor.getInt(cursor.getColumnIndex("codMotivo")),
                    setStatus(cursor.getInt(cursor.getColumnIndex("flRetorno")),
                        cursor.getInt(cursor.getColumnIndex("flPresente"))),
                    cursor.getString(cursor.getColumnIndex("desCategoria")),
                    cursor.getString(cursor.getColumnIndex("desMarca")),
                    cursor.getString(cursor.getColumnIndex("desSku")),
                    selectFotosEnvio( cursor.getInt(cursor.getColumnIndex("id")))
                )
                lista.add(validadeSpotValidity)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun selectValidade(codPesquisa: Int) : ArrayList<ValidadeSpotValidity> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           p.*  , tp.desCategoria, tp.desMarca, tp.desSku                                               " +
                    "   FROM TB_Coleta_Validade p inner join TB_Produto tp on tp.codCategoria = p.codCategoria" +
                    " and   tp.codMarca = p.codMarca and tp.codSku = p.codSku   " +
                    " WHERE codPesquisa in (?)                                                               " +
                    "   ORDER BY desMarca                                                                    "
        )

        cursor = this.executaQuery(sbQuery.toString(), codPesquisa)

        val lista = ArrayList<ValidadeSpotValidity>()
        if (cursor.moveToFirst())
            do {
                val validadeSpotValidity = ValidadeSpotValidity(
                    cursor.getInt(cursor.getColumnIndex("id")),
                    cursor.getInt(cursor.getColumnIndex("codPesquisa")),
                    cursor.getInt(cursor.getColumnIndex("codCategoria")),
                    cursor.getInt(cursor.getColumnIndex("codMarca")),
                    cursor.getInt(cursor.getColumnIndex("codSku")),
                    cursor.getInt(cursor.getColumnIndex("quantidade")),
                    cursor.getString(cursor.getColumnIndex("lote")),
                    cursor.getString(cursor.getColumnIndex("data_validade")),
                    cursor.getString(cursor.getColumnIndex("observacoes")),
                    cursor.getInt(cursor.getColumnIndex("flRetorno")),
                    cursor.getInt(cursor.getColumnIndex("flPresente")),
                    cursor.getInt(cursor.getColumnIndex("codMotivo")),
                    setStatus(cursor.getInt(cursor.getColumnIndex("flRetorno")),
                        cursor.getInt(cursor.getColumnIndex("flPresente"))),
                    cursor.getString(cursor.getColumnIndex("desCategoria")),
                    cursor.getString(cursor.getColumnIndex("desMarca")),
                    cursor.getString(cursor.getColumnIndex("desSku")),
                    selectFotos( cursor.getInt(cursor.getColumnIndex("id")))
                     )
                lista.add(validadeSpotValidity)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    private fun setStatus(flRetorno: Int, flPresente: Int): Int {
        if(flRetorno == 0)
            return 2
        else if(flRetorno == 1) {
            return if(flPresente >= 0)
                2
            else
                0
        }

        return 0
    }

    private fun selectFotosEnvio(idColeta: Int): List<String> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           foto                                              " +
                    "   FROM TB_Coleta_Validade_Fotos p   " +
                    " WHERE idColetaValidade in (?)                                                               "
        )

        cursor = this.executaQuery(sbQuery.toString(), idColeta)

        val lista = ArrayList<String>()
        if (cursor.moveToFirst())
            do {
                lista.add(UtilidadesValidity.retonarNomeFotoValidity(cursor.getString(cursor.getColumnIndex("foto"))))
            } while (cursor.moveToNext())

        cursor.close()

        return lista

    }

    private fun selectFotos(idColeta: Int): List<String> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           foto                                              " +
                    "   FROM TB_Coleta_Validade_Fotos p   " +
                    " WHERE idColetaValidade in (?)                                                               "
        )

        cursor = this.executaQuery(sbQuery.toString(), idColeta)

        val lista = ArrayList<String>()
        if (cursor.moveToFirst())
            do {
                lista.add(cursor.getString(cursor.getColumnIndex("foto")))
            } while (cursor.moveToNext())

        cursor.close()

        return lista

    }

    fun validaColetaValidade(codPesquisa: Int) : Boolean {
        return if(selectValidade(codPesquisa).size >0 ) {
                selectValidade(codPesquisa).all { it.status == 2 }
        } else {
            false
        }
    }

    fun selectPessoas(nivel: Int) : ArrayList<ValidadeSpotValidity>{
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           distinct p.codPessos, p.desPessoa                                              " +
                    "   FROM TB_Produtos_A_Vencer p   " +
                    " order by p.desPessoa                                                              "
        )

        cursor = this.executaQuery(sbQuery.toString())

        val lista = ArrayList<ValidadeSpotValidity>()
        if (cursor.moveToFirst())
            do {
                val validaColetaValidade = ValidadeSpotValidity()
                validaColetaValidade.codPessoa = cursor.getInt(cursor.getColumnIndex("codPessoa"))
                validaColetaValidade.desPessoa = cursor.getString(cursor.getColumnIndex("desPessoa"))
                validaColetaValidade.nivel = nivel

                lista.add(validaColetaValidade)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun selectLojas(nivel: Int, codPessoa: Int) : ArrayList<ValidadeSpotValidity>{
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           distinct p.codPessos, p.desPessoa, codLoja, desLoja                                              " +
                    "   FROM TB_Produtos_A_Vencer p  where codPessoa = ?  " +
                    " order by p.desLoja                                                              "
        )

        cursor = this.executaQuery(sbQuery.toString(), codPessoa)

        val lista = ArrayList<ValidadeSpotValidity>()
        if (cursor.moveToFirst())
            do {
                val validaColetaValidade = ValidadeSpotValidity()
                validaColetaValidade.codPessoa = cursor.getInt(cursor.getColumnIndex("codPessoa"))
                validaColetaValidade.desPessoa = cursor.getString(cursor.getColumnIndex("desPessoa"))
                validaColetaValidade.codLoja = cursor.getInt(cursor.getColumnIndex("codLoja"))
                validaColetaValidade.desLoja = cursor.getString(cursor.getColumnIndex("desLoja"))
                validaColetaValidade.nivel = nivel

                lista.add(validaColetaValidade)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun selectProdutos(nivel: Int, codPessoa: Int, codLoja: Int): ArrayList<ValidadeSpotValidity> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           *                                             " +
                    "   FROM TB_Produtos_A_Vencer p  where codPessoa = ? and codLoja = ? " +
                    " order by p.data_validade                                                          "
        )

        cursor = this.executaQuery(sbQuery.toString(), codPessoa, codLoja)

        val lista = ArrayList<ValidadeSpotValidity>()
        if (cursor.moveToFirst())
            do {
                val validaColetaValidade = ValidadeSpotValidity()
                validaColetaValidade.codPessoa = cursor.getInt(cursor.getColumnIndex("id"))
                validaColetaValidade.codPesquisa = cursor.getInt(cursor.getColumnIndex("codPesquisa"))
                validaColetaValidade.codCategoria = cursor.getInt(cursor.getColumnIndex("codCategoria"))
                validaColetaValidade.codMarca = cursor.getInt(cursor.getColumnIndex("codMarca"))
                validaColetaValidade.codSku = cursor.getInt(cursor.getColumnIndex("codSku"))
                validaColetaValidade.quantidade = cursor.getInt(cursor.getColumnIndex("quantidade"))
                validaColetaValidade.lote = cursor.getString(cursor.getColumnIndex("lote"))
                validaColetaValidade.data_validade = cursor.getString(cursor.getColumnIndex("data_validade"))
                validaColetaValidade.observacoes = cursor.getString(cursor.getColumnIndex("observacoes"))
                validaColetaValidade.desCategoria = cursor.getString(cursor.getColumnIndex("desCategoria"))
                validaColetaValidade.desMarca = cursor.getString(cursor.getColumnIndex("desMarca"))
                validaColetaValidade.desSku = cursor.getString(cursor.getColumnIndex("desSku"))
                validaColetaValidade.codPessoa = cursor.getInt(cursor.getColumnIndex("codPessoa"))
                validaColetaValidade.desPessoa = cursor.getString(cursor.getColumnIndex("desPessoa"))
                validaColetaValidade.codLoja = cursor.getInt(cursor.getColumnIndex("codLoja"))
                validaColetaValidade.desLoja = cursor.getString(cursor.getColumnIndex("desLoja"))
                validaColetaValidade.nivel = nivel

                lista.add(validaColetaValidade)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun updateRetorno(idColeta: Int, codMotivo: Int?, flPresente: Int) {

        val sbQuery = StringBuilder()
        sbQuery.append(
            "UPDATE TB_Coleta_Validade SET codMotivo = ? , flPresente = ? WHERE id = ? "
        )

        this.executaNoQuery(sbQuery.toString(), codMotivo, flPresente, idColeta)
    }
}