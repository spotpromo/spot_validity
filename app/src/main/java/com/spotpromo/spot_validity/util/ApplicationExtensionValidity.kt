package com.spotpromo.spot_validity.util

import android.app.Application
import com.spotpromo.spot_validity.di.ValidadeModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

fun Application.startKoinValidity() {
    startKoin {
        androidContext(this@startKoinValidity)
        androidLogger()
        ValidadeModule.loadModules()
    }
}