package com.spotpromo.spot_validity.util

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ortiz.touchview.TouchImageView
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.di.ValidadeModule
import com.spotpromo.spot_validity.model.CodeData
import com.spotpromo.spot_validity.model.Produto
import com.spotpromo.spotcamera.SpotCamera
import com.spotpromo.spotcamera.config.SpotCameraConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

object UtilidadesValidity {
    @Throws(Exception::class)
    fun validaSpinnerMarca(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codMarca == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerCategoria(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codCategoria == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    /**
     * VALIDA SPINNER
     *
     * @param spn
     * @return
     */
    @Throws(Exception::class)
    fun validaSpinnerSku(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as Produto

        if (produto.codSku == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    @Throws(Exception::class)
    fun validaSpinnerCodeData(spn: Spinner, context: Context): Boolean {
        val produto = spn.selectedItem as CodeData

        if (produto.codigo == 0) {

            val subView = spn.getChildAt(0)
            if (subView is LinearLayout) {
                val txt = subView.getChildAt(0) as TextView
                txt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
                txt.setTextColor(Color.RED)
                txt.text = context.resources.getString(R.string.msg_campo_obrigatorio)
            }

            return false
        }

        return true
    }

    @Throws(Exception::class)
    fun validaEditText(edt: EditText, context: Context): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
            return false
        }

        return true
    }


    @Throws(Exception::class)
    fun validaEditTextInteiro(edt: EditText, context: Context, valida_zero: Boolean): Boolean {
        if (edt.text.toString().isEmpty()) {
            edt.error = context.resources.getString(R.string.msg_campo_obrigatorio)
            return false
        }

        if (valida_zero && edt.text.toString().toInt() == 0) {
            edt.error = context.resources.getString(R.string.msg_valor_maior_zero)
            return false
        }

        return true
    }


    @Throws(Exception::class)
    fun showImageGallery(context: Context, url: String, vB: View?) {

        val params =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                1f
            )

        val mDialog: AlertDialog?
        val builder = AlertDialog.Builder(context, R.style.MyAlertDialogStyleDialog)

        val inflater = (context as Activity).layoutInflater
        val dialogView = inflater.inflate(R.layout.layout_dialog_show_image_validity, null)

        val timageView = dialogView.findViewById(R.id.timageproduto) as TouchImageView
        val fechar = dialogView.findViewById(R.id.fechar) as ImageView

        CarregaImagemDaUrl.carregaImagen(context, timageView, url)

        builder.setView(dialogView)

        mDialog = builder.create()
        mDialog!!.show()

        fechar.setOnClickListener {
            if (vB != null)
                vB.isClickable = true

            if (mDialog != null && mDialog.isShowing)
                mDialog.dismiss()
        }
    }

    @Throws(java.lang.Exception::class)
    fun retornaCaminhoFotoURINew(
        uriAtual: Uri?,
        codRoteiro: Int,
        nomefoto: String?,
        context: Context
    ): String? {
        var retorna_novo_caminho_ou_antigo = ""
        val sdcard = SpotValidityConfig.principal_path

        /** PASTA PROJETO  */
        val pasta_projeto = File(
            sdcard!! + File.separator + context.resources
                .getString(R.string.db_name_directory)
        )
        if (!pasta_projeto.isDirectory) pasta_projeto.mkdirs()
        /** PASTA SPOT FOTOS  */
        val pasta_spot = File(
            pasta_projeto.absolutePath + File.separator + context.resources
                .getString(R.string.name_directory_fotos)
        )
        if (!pasta_spot.isDirectory) pasta_spot.mkdirs()
        val pasta_roteiro =
            File(pasta_spot.absolutePath + File.separator + codRoteiro)
        if (!pasta_roteiro.isDirectory) pasta_roteiro.mkdirs()
        val stringi_file_copy: String = CriarFileTemporarioStringRoteiro(
            pasta_roteiro.absolutePath,
            nomefoto!!,
            codRoteiro
        )
        val file_copy = File(stringi_file_copy)

        /** COPIAR ARQUIVO PARA NOVA PASTA  */
        var `in`: InputStream? = null
        var out: OutputStream? = null
        try {
            `in` = context.contentResolver.openInputStream(uriAtual!!)
            out = FileOutputStream(file_copy)

            val buf = ByteArray(1024)
            var len: Int
            while (`in`!!.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }

            if (file_copy.isFile) retorna_novo_caminho_ou_antigo = file_copy.absolutePath
        } finally {
            `in`?.close()
            out?.close()
        }

        val file_movido = File(retorna_novo_caminho_ou_antigo)
        if (!file_movido.isFile)
            throw Exception("A foto não foi atualizada, por favor tente novamente")

        return retorna_novo_caminho_ou_antigo

    }

    @Throws(java.lang.Exception::class)
    fun CriarFileTemporarioStringRoteiro(
        diretorio: String,
        nomefoto: String,
        codRoteiro: Int
    ): String {
        val timeStamp =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault())
                .format(Date())
        val mediaFile: File
        mediaFile =
            File(diretorio + File.separator + nomefoto + "_" + codRoteiro + "_" + timeStamp + ".jpg")
        return mediaFile.absolutePath
    }

    fun abrirCameraNova(
        activity: Activity,
        tipo: Int,

    ){
        try {
            val tamanho = SpotValidityConfig.tamanho

            SpotCameraConfig.buttonColor = SpotValidityConfig.color
            SpotCameraConfig.photoSize = tamanho
            SpotCameraConfig.tempFilePath = activity.getExternalFilesDir(null).toString() + "/temp_file.jpeg"
            SpotCameraConfig.isWithGallery = false

            activity.startActivityForResult(
                Intent(activity, SpotCamera::class.java).putExtra("tipo",tipo), tipo
            )

        } catch (e: Exception){
            LogTrace.logCatch(activity, this.javaClass, e, true)
        }
    }

    fun startKoinApp(context: Context) {
        startKoin{
            androidContext(context)
            androidLogger()
            ValidadeModule.loadModules()
        }
    }

    fun retonarNomeFotoValidity(caminhoFoto: String?): String {
        if (caminhoFoto == null || caminhoFoto.toString() == "") return ""
        val foto = caminhoFoto.split("/").toTypedArray()
        return foto[foto.size - 1]
    }

}