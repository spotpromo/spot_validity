package com.spotpromo.spot_validity.util.dao

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.spotpromo.spot_validity.model.CodeData
import com.spotpromo.spot_validity.util.DAOHelper
import java.util.ArrayList

class CodeDataDAOHelper @Throws(Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {

    /**
     * SELECT
     * @return
     */
    @Throws(Exception::class)
    fun select(tipo: String): ArrayList<CodeData> {
        var cursor: Cursor? = null

        val sbQuery = StringBuilder()

        sbQuery.append("SELECT * FROM TB_CodeData WHERE tipo IN (?) ORDER BY descricao ASC ")

        cursor = this.executaQuery(sbQuery.toString(), tipo)
        val lista = ArrayList<CodeData>()
        lista.add(selecione())
        if (cursor.moveToFirst()) {
            do {
                var codeData = CodeData()
                codeData.codigo = cursor.getInt(cursor.getColumnIndex("codigo"))
                codeData.tipo = cursor.getString(cursor.getColumnIndex("tipo"))
                codeData.descricao = cursor.getString(cursor.getColumnIndex("descricao"))
                lista.add(codeData)
            } while (cursor.moveToNext())
        }

        cursor.close()

        return lista
    }

    fun selecione() : CodeData {
        val codedata = CodeData()
        codedata.codigo = 0
        codedata.descricao = "SELECIONE"

        return codedata
    }
}