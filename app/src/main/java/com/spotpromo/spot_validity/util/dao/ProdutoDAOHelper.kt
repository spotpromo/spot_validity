package com.spotpromo.spot_validity.util.dao

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.spotpromo.spot_validity.model.Produto
import com.spotpromo.spot_validity.util.DAOHelper
import java.util.ArrayList

class ProdutoDAOHelper @Throws(Exception::class)
constructor(db: SQLiteDatabase) : DAOHelper(db) {


    fun selecioneSku(): Produto {
        val produto = Produto()
        produto.codCategoria = 0
        produto.desCategoria = "SELECIONE"
        produto.codMarca = 0
        produto.desMarca = "SELECIONE"
        produto.codSku = 0
        produto.desSku = "SELECIONE"
        produto.flConcorrente = 0

        return produto
    }

    fun selecioneMarca(): Produto {
        val produto = Produto()
        produto.codCategoria = 0
        produto.desCategoria = "SELECIONE"
        produto.codMarca = 0
        produto.desMarca = "SELECIONE"
        produto.flConcorrente = 0

        return produto
    }

    fun selecioneCategoria(): Produto {
        val produto = Produto()
        produto.codCategoria = 0
        produto.desCategoria = "SELECIONE"
        produto.flConcorrente = 0

        return produto
    }

    fun selectSkuSpotValidity(codCategoria: Int, codMarca: Int): ArrayList<Produto> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           DISTINCT codCategoria, desCategoria, codMarca, desMarca, codSku, desSku                                                  " +
                    "   FROM TB_Produto p      " +
                    " WHERE codCategoria in (?) and codMarca in (?)                                                              " +
                    "   ORDER BY desMarca                                                                    "
        )

        cursor = this.executaQuery(sbQuery.toString(), codCategoria, codMarca)

        val lista = ArrayList<Produto>()
        lista.add(selecioneSku())
        if (cursor.moveToFirst())
            do {
                val produto = Produto()
                produto.codCategoria = cursor.getInt(cursor.getColumnIndex("codCategoria"))
                produto.desCategoria = cursor.getString(cursor.getColumnIndex("desCategoria"))
                produto.codMarca = cursor.getInt(cursor.getColumnIndex("codMarca"))
                produto.desMarca = cursor.getString(cursor.getColumnIndex("desMarca"))
                produto.codSku = cursor.getInt(cursor.getColumnIndex("codSku"))
                produto.desSku = cursor.getString(cursor.getColumnIndex("desSku"))
                lista.add(produto)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun selectMarcasSpotValidity(codCategoria: Int): ArrayList<Produto> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           DISTINCT codCategoria, desCategoria, codMarca, desMarca                                                  " +
                    "   FROM TB_Produto p      " +
                    " WHERE codCategoria in (?)                                                               " +
                    "   ORDER BY desMarca                                                                    "
        )

        cursor = this.executaQuery(sbQuery.toString(), codCategoria)

        val lista = ArrayList<Produto>()
        lista.add(selecioneMarca())
        if (cursor.moveToFirst())
            do {
                val produto = Produto()
                produto.codCategoria = cursor.getInt(cursor.getColumnIndex("codCategoria"))
                produto.desCategoria = cursor.getString(cursor.getColumnIndex("desCategoria"))
                produto.codMarca = cursor.getInt(cursor.getColumnIndex("codMarca"))
                produto.desMarca = cursor.getString(cursor.getColumnIndex("desMarca"))
                lista.add(produto)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }

    fun selectCategoriasSpotValidity(): ArrayList<Produto> {
        val cursor: Cursor?
        val sbQuery = StringBuilder()
        sbQuery.append(
            "" +
                    "   SELECT                                                                               " +
                    "           DISTINCT p.codCategoria, p.desCategoria                                                  " +
                    "   FROM TB_Produto p                                                                    " +
                    "   ORDER BY desCategoria                                                                    "
        )

        cursor = this.executaQuery(sbQuery.toString())

        val lista = ArrayList<Produto>()
        lista.add(selecioneCategoria())
        if (cursor.moveToFirst())
            do {
                val produto = Produto()
                produto.codCategoria = cursor.getInt(cursor.getColumnIndex("codCategoria"))
                produto.desCategoria = cursor.getString(cursor.getColumnIndex("desCategoria"))
                produto.flConcorrente = 0
                lista.add(produto)
            } while (cursor.moveToNext())

        cursor.close()

        return lista
    }
}