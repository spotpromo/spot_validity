package com.spotpromo.spot_validity.view.coleta

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.SendIntent
import com.spotpromo.spot_validity.view.lista.ActivityListaValidade
import com.spotpromo.spot_validity.viewmodel.ColetaValidadeViewModel
import kotlinx.android.synthetic.main.activity_coleta_validade_spot_validity.*
import kotlinx.android.synthetic.main.layout_lista_fotos_spot_validity.view.*
import kotlinx.android.synthetic.main.layout_recycler_buttom_spot_validity.view.*
import org.koin.android.ext.android.inject

class ActivityColetaValidadeSpotValidity : AppCompatActivity(R.layout.activity_coleta_validade_spot_validity) {
    var codPesquisa = 0
    var codRoteiro = 0
    var mColetaValidade : ValidadeSpotValidity? = null
    val viewModel: ColetaValidadeViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpRetorno()
    }

    private fun setUpComponents() {
        ToolbarColetaValidadeSpotValidity.loadToolbar(SpotValidityConfig.app_name, getString(R.string.validade_spot_validity),
        this, ActivityListaValidade::class.java, codPesquisa, codRoteiro)
        SelecioneSku.loadComponents(this)
        ValidityFields.loadComponents(this)
        setUpClick()
    }

    private fun setUpRetorno() {
        val bundle = intent.getBundleExtra("bundle")
        if (bundle != null) {
            mColetaValidade = bundle.getSerializable("mColetaValidade") as ValidadeSpotValidity?
            codRoteiro = bundle.getInt("codRoteiro")
            codPesquisa = bundle.getInt("codPesquisa")
        }

        setUpComponents()

        Fotos.loadComponents(this, codRoteiro)
        if(mColetaValidade != null)
            setUpRetornoScreen()
    }

    private fun setUpRetornoScreen() {
        mColetaValidade?.let {
            SelecioneSku.setUpRetorno(it)
            ValidityFields.setUpRetorno(it)
            Fotos.setUpRetorno(it)
        }
    }

    private fun setUpClick() {
        Salvar.backgroundTintList = ColorStateList.valueOf(Color.parseColor(SpotValidityConfig.color))
        Salvar.setOnClickListener {
            if(SelecioneSku.validateSpinners(this) && ValidityFields.validateFields(this)
                && Fotos.validaFotos(this))
                    gravar()
        }
    }

    private fun gravar() {
        val coletaValidadeSpotValidity = ValidadeSpotValidity()
        coletaValidadeSpotValidity.codPesquisa = codPesquisa
        coletaValidadeSpotValidity.id = if(mColetaValidade != null) mColetaValidade!!.id else 0
        coletaValidadeSpotValidity.codCategoria = SelecioneSku.getCategoria().codCategoria!!
        coletaValidadeSpotValidity.codMarca = SelecioneSku.getMarca().codMarca!!
        coletaValidadeSpotValidity.codSku = SelecioneSku.getSku().codSku!!
        coletaValidadeSpotValidity.quantidade = ValidityFields.getQtd()
        coletaValidadeSpotValidity.lote = ValidityFields.getLote()
        coletaValidadeSpotValidity.data_validade = ValidityFields.getDataVencimento()
        coletaValidadeSpotValidity.observacoes = ValidityFields.getObservacoes()
        coletaValidadeSpotValidity.lista_foto = Fotos.getListaFotos()
        coletaValidadeSpotValidity.flRetorno = 0
        coletaValidadeSpotValidity.flPresente = 0
        coletaValidadeSpotValidity.codMotivo = 0

        viewModel.insertColeta(coletaValidadeSpotValidity, this)

        onBackPressed()
    }

    override fun onBackPressed() {
        val bundle = Bundle()
        bundle.putInt("codRoteiro", codRoteiro)
        bundle.putInt("codPesquisa", codPesquisa!!)
        SendIntent.with()
            .mClassFrom(this)
            .mBundle(bundle)
            .mClassTo(ActivityListaValidade::class.java)
            .mType(R.integer.slide_from_left)
            .go()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Fotos.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}