package com.spotpromo.spot_validity.view.skuvencimento

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.util.SendIntent
import com.spotpromo.spot_validity.view.lista.ActivityListaValidade
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.activity_lista_produtos_a_vencer_spot_validity.*
import kotlinx.android.synthetic.main.activity_lista_validade_spot_validity.ToolbarSpotValidity
import org.koin.android.ext.android.inject

class ActivityListaProdutosAVencer : AppCompatActivity(R.layout.activity_lista_produtos_a_vencer_spot_validity) {
    val viewModel: ListaValidadeViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpComponents()
    }

    private fun setUpComponents() {
        ToolbarSpotValidity.loadToolbar(SpotValidityConfig.app_name, getString(R.string.produtos_a_avenver), this, SpotValidityConfig.classTo, 0, 0)
        ListaProdutos.loadComponents(this, viewModel)
    }

    override fun onBackPressed() {
        when(ListaProdutos.getNivel()){
            2,1 -> {
                ListaProdutos.setNivel(ListaProdutos.getNivel() -1)
                ListaProdutos.loadComponents(this, viewModel)
            }
            else -> {
                SendIntent.with()
                    .mClassFrom(this)
                    .mClassTo(SpotValidityConfig.classTo)
                    .mType(R.integer.slide_from_left)
                    .go()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }
}