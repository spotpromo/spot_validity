package com.spotpromo.spot_validity.view.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.AdapterCodeData
import com.spotpromo.spot_validity.model.CodeData
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.LogTrace
import com.spotpromo.spot_validity.util.SqliteDataBaseHelper
import com.spotpromo.spot_validity.util.UtilidadesValidity
import com.spotpromo.spot_validity.util.dao.CodeDataDAOHelper
import com.spotpromo.spot_validity.util.dao.ColetaValidadeSpotValidityDAOHelper
import kotlinx.android.synthetic.main.layout_fragmento_validade_retorno.*

class FragmentDialogRetorno : DialogFragment(), DialogInterface.OnShowListener {
    private lateinit var mAlertDialog: AlertDialog
    private lateinit var mView: View
    private var TAG: String = "FRAGMENTO_VALIDADE_EXISTE"
    private lateinit var btnGravar: Button
    private lateinit var btnFechar: Button
    private var vButton: View? = null
    private var listaMotivo = ArrayList<CodeData>()
    private var validadeSpotValidity : ValidadeSpotValidity? = null
    private var position = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity!!, R.style.MyAlertDialogStyle_FragmentoDialogo)
            .setIcon(null)
            .setNegativeButton(activity!!.resources.getString(R.string.btn_cancelar), null)


        builder.setPositiveButton(activity!!.resources.getString(R.string.btn_salvar), null)
        builder.setTitle(activity!!.resources.getString(R.string.coleta_validade_existe))

        mAlertDialog = builder.create()
        mAlertDialog.setOnShowListener(this)
        return mAlertDialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.layout_fragmento_validade_retorno, container, false)
        mAlertDialog.setView(mView)
        return mView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            controles()
        } catch (err: Exception) {
            LogTrace.logCatch(context!!, context!!.javaClass, err, true)
        }
    }

    private fun controles() {
        listaMotivo.addAll(CodeDataDAOHelper(SqliteDataBaseHelper.openDB(context!!)).select("MOTIVO_VALIDADE"))
        spn_justificativa_validade_validity.adapter = AdapterCodeData(context!! ,listaMotivo)

        s_ainda_existe_validade.setOnCheckedChangeListener { buttonView, isChecked ->
            ln_motivo.visibility = if(isChecked) View.VISIBLE else View.GONE
        }

        setUpRetorno()
    }

    private fun setUpRetorno() {
       arguments?.let {
           validadeSpotValidity = it.getSerializable("mColetaValidade") as ValidadeSpotValidity
           position = it.getInt("position")

           validadeSpotValidity?.let {
               s_ainda_existe_validade.isChecked = it.flPresente == 1
               for(i in listaMotivo.indices)
                   if(listaMotivo[i].codigo == it.codMotivo)
                       spn_justificativa_validade_validity.setSelection(i)
           }
       }
    }

    fun show(manager: FragmentManager, v: View) {
        try {
            vButton = v
            val fragmentoTransaction = manager.beginTransaction()
            fragmentoTransaction.add(this, TAG)
            fragmentoTransaction.commitAllowingStateLoss()
        } catch (err: Exception) {
            LogTrace.logCatch(context!!, context!!.javaClass, err, true)
        }
    }

    override fun onShow(dialog: DialogInterface?) {
        btnFechar = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
        btnFechar.setOnClickListener {
            dismiss()
        }
        btnGravar = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        btnGravar.setOnClickListener {
            if(validaCampos())
                gravar()
        }
    }

    private fun validaCampos() : Boolean {
        return UtilidadesValidity.validaSpinnerCodeData(
            spn_justificativa_validade_validity,
            context!!
        ) && ln_motivo.visibility == View.VISIBLE
    }

    private fun gravar() {
        ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context!!)).updateRetorno(
            validadeSpotValidity!!.id!!,
            (spn_justificativa_validade_validity.selectedItem as CodeData).codigo,
            if(s_ainda_existe_validade.isChecked) 1 else 0
        )

        validadeSpotValidity!!.status = 2
        val updatePosition = context as updatePosition
        updatePosition.notify(position)
    }

    interface updatePosition{
        fun notify(position: Int)
    }
}