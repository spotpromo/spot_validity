package com.spotpromo.spot_validity.view.lista

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.di.ValidadeModule
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.SendIntent
import com.spotpromo.spot_validity.util.startKoinValidity
import com.spotpromo.spot_validity.view.fragment.FragmentDialogRetorno
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.activity_lista_validade_spot_validity.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class ActivityListaValidade : AppCompatActivity(R.layout.activity_lista_validade_spot_validity),
    FragmentDialogRetorno.updatePosition {
    val viewModel: ListaValidadeViewModel by inject()
    var codPesquisa = 0
    var codRoteiro = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpComponents()
    }

    private fun setUpComponents() {
        val bundle = intent.getBundleExtra("bundle")
        if (bundle != null) {
            codPesquisa = bundle.getInt("codPesquisa")
            codRoteiro = bundle.getInt("codRoteiro")
        }

        ToolbarSpotValidity.loadToolbar(SpotValidityConfig.app_name, getString(R.string.validade_spot_validity), this, SpotValidityConfig.classTo, codPesquisa, codRoteiro)
        ListaValidade.loadComponents(this, viewModel, codPesquisa, codRoteiro)
    }

    override fun onBackPressed() {
        SendIntent.with()
            .mClassFrom(this)
            .mClassTo(SpotValidityConfig.classTo)
            .mType(R.integer.slide_from_left)
            .go()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun notify(position: Int) {
        ListaValidade.notifyItem(position)
    }
}