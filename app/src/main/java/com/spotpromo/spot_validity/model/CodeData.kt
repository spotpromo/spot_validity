package com.spotpromo.spot_validity.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CodeData : Serializable {

    @SerializedName("codigo")
     var codigo: Int? = null
    @SerializedName("codigo2")
    var codigo2: Int? = null
    @SerializedName("tipo")
     var tipo: String? = null
    @SerializedName("descricao")
     var descricao: String? = null
    @SerializedName("descricao2")
    var descricao2: String? = null

    var isSelected : Boolean = false
}