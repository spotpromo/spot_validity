package com.spotpromo.spot_validity.model

import java.io.Serializable

class ColetaSpotValidity: Serializable {
    var codPesquisa : Int? = null
    var id : Int? = null
    var codCategoria: Int? = null
    var codMarca : Int? = null
    var codSku : Int? = null
    var quantidade : Int? = null
    var lote : String? = null
    var data_validade : String? = null
    var observacoes : String? = null

    var lista_foto : ArrayList<String>? = null
}