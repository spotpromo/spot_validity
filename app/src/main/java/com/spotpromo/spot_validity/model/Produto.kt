package com.spotpromo.spot_validity.model
import java.io.Serializable

class Produto : Serializable {
    var codSubBu: Int? = null

    var codMarca: Int? = null
    var desMarca: String? = null
    var codCategoria: Int? = null

    var desCategoria: String? = null
    var codSubCategoria: Int? = null
    var desSubCategoria: String? = null
    var codSku: Int? = null
    var desSku: String? = null
    var precoSugerido: String? = null

    var flConcorrente: Int? = null

    var urlProduto: String? = null
    var ordemShareCategoria: Int? = null

    var ordem: Int? = null

    var status: Int? = null
    var nivel: Int? = null
    var isSelected: Boolean = false

}