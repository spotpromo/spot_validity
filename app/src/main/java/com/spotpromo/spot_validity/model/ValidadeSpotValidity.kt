package com.spotpromo.spot_validity.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
class ValidadeSpotValidity(
    @Json(name = "id") var id: Int? = null,
    @Json(name = "codPesquisa") var codPesquisa: Int? = null,
    @Json(name = "codCategoria") var codCategoria: Int? = null,
    @Json(name = "codMarca") var codMarca: Int? = null,
    @Json(name = "codSku") var codSku: Int? = null,
    @Json(name = "quantidade") var quantidade: Int? = null,
    @Json(name = "lote") var lote: String? = null,
    @Json(name = "data_validade") var data_validade: String? = null,
    @Json(name = "observacoes") var observacoes: String? = null,
    @Json(name = "flRetorno") var flRetorno: Int? = null,
    @Json(name = "flPresente") var flPresente: Int? = null,
    @Json(name = "codMotivo") var codMotivo: Int? = null,
    @Json(name = "status") var status: Int? = null,
    @Json(name = "desCategoria") var desCategoria: String? = null,
    @Json(name = "desMarca") var desMarca: String? = null,
    @Json(name = "desSku") var desSku: String? = null,
    @Json(name = "LISTA_FOTO_VALIDADE") var lista_foto: List<String>? = null,
    @Json(name = "codPessoa") var codPessoa: Int? = null,
    @Json(name = "desPessoa") var desPessoa: String? = null,
    @Json(name = "codLoja") var codLoja: Int? = null,
    @Json(name = "desLoja") var desLoja: String? = null,
    @Json(name = "nivel") var nivel: Int? = null
) : Parcelable, Serializable