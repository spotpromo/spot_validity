package com.spotpromo.spot_validity.components

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.Alerta
import com.spotpromo.spot_validity.util.SendIntent
import com.spotpromo.spot_validity.util.SqliteDataBaseHelper
import com.spotpromo.spot_validity.util.dao.ColetaValidadeSpotValidityDAOHelper
import com.spotpromo.spot_validity.view.coleta.ActivityColetaValidadeSpotValidity
import com.spotpromo.spot_validity.view.fragment.FragmentDialogRetorno
import kotlinx.android.synthetic.main.recycle_generic_valdity.view.*

class RecyclerListaValidadeSpotValidity  (val items: ArrayList<ValidadeSpotValidity>, val activity: AppCompatActivity, val codRoteiro: Int) : RecyclerView.Adapter<RecyclerListaValidadeSpotValidity.ViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_generic_valdity, parent, false)

        return ViewModel(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val item = items[position]
        with(holder.itemView) {

            var sbHtml = StringBuilder()

            val color: String =
                when (item.status) {
                    0 -> context.resources.getString(R.string.chamado_vermelho)
                    1 -> context.resources.getString(R.string.chamado_amarelo)
                    else ->
                        context.resources.getString(R.string.chamado_verde)
                }

            when (item.status) {
                0 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.vermelho))
                1 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.amarelo))
                2 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.verde))
            }

            sbHtml.append(
                String.format(
                    "<br>Categoria: <b><font color='$color'>%s</font><b/></br>",
                    item.desCategoria
                )
            )
            sbHtml.append(
                String.format(
                    "<br>Marca: <b><font color='$color'>%s</font><b/></br>",
                    item.desMarca
                )
            )
            sbHtml.append(
                String.format(
                    "<br>Sku: <b><font color='$color'>%s</font><b/></br>",
                    item.desSku
                )
            )
            sbHtml.append(
                String.format(
                    "<br>Lote: <b><font color='$color'>%s</font><b/></br>",
                    item.lote
                )
            )
            sbHtml.append(
                String.format(
                    "<br>Data de Vencimento: <b><font color='$color'>%s</font><b/></br>",
                    item.data_validade
                )
            )

            if (item.flRetorno == 1) {
                sbHtml.append(
                    String.format(
                        "<br></br><br><b><font color='$color'>%s</font><b/></br>",
                        "Retorno"
                    )
                )
            }

            txt_dados.text = HtmlCompat.fromHtml(sbHtml.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT)
            mrow.setOnClickListener {
                if(item.flRetorno == 0) {
                    Alerta.show(activity, activity.getString(R.string.editar_excluir), activity.getString(R.string.o_que_deseja),
                        activity.getString(R.string.editar), { dialog, which ->
                            val bundle = Bundle()
                            bundle.putSerializable("mColetaValidade", item)
                            bundle.putInt("codRoteiro", codRoteiro)
                            bundle.putInt("codPesquisa", item.codPesquisa!!)

                            SendIntent.with()
                                .mClassFrom(activity)
                                .mBundle(bundle)
                                .mType(R.integer.slide_from_left)
                                .mClassTo(ActivityColetaValidadeSpotValidity::class.java)
                                .go()
                        }, activity.getString(R.string.excluir),
                        { dialog, which ->
                            ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(activity)).deletarValidade(item.id!!)
                            items.removeAt(position)
                            notifyDataSetChanged()
                        },activity.getString(R.string.btn_cancelar),
                        { dialog, which ->  dialog.dismiss() },
                        false)
                } else {
                    val bundle = Bundle()
                    bundle.putSerializable("mColetaValidade", item)
                    bundle.putInt("position", position)

                    val fragment = FragmentDialogRetorno()
                    fragment.arguments = bundle
                    fragment.isCancelable = false
                    fragment.show(activity.supportFragmentManager, it)
                }
            }
        }
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view)
}