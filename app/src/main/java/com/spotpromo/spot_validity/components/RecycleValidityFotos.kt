package com.spotpromo.spot_validity.components

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.util.LogTrace
import com.spotpromo.spot_validity.util.UtilidadesValidity
import java.io.File
import java.util.*


class RecycleValidityFotos(private val mContext: Context, private val mLista: ArrayList<String>) :
    RecyclerView.Adapter<RecycleValidityFotos.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        internal var imgFoto: ImageView
        internal var mRow: LinearLayout
        internal var txtFechar: TextView

        init {
            txtFechar = v.findViewById<View>(R.id.txt_fechar) as TextView
            mRow = v.findViewById<View>(R.id.mrow) as LinearLayout
            imgFoto = v.findViewById<View>(R.id.img_foto) as ImageView
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.recycle_foto_generic_validity, viewGroup, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, i: Int) {
        try {
            val fotoValidity = mLista[i]

            val myBitmap = BitmapFactory.decodeFile(fotoValidity)



            holder.imgFoto.setImageBitmap(myBitmap)

            holder.txtFechar.setOnClickListener(onClickListenerFechar(i))
            holder.imgFoto.setOnClickListener(onClickListenerAbrir(i, holder.imgFoto))

        } catch (err: Exception) {
            LogTrace.logCatch(mContext, mContext.javaClass, err, true)
        }
    }

    /**
     * METODO ONCLICK
     *
     * @param position
     * @return
     */
    private fun onClickListenerFechar(position: Int): View.OnClickListener {
        return View.OnClickListener { v ->
            try {
                mLista.removeAt(position)
                notifyDataSetChanged()

            } catch (err: Exception) {
                LogTrace.logCatch(v.context, v.context.javaClass, err, true)
            }
        }
    }
    /**
     * METODO ONCLICK
     *
     * @param position
     * @return
     */
    private fun onClickListenerAbrir(position: Int, view : View): View.OnClickListener {
        return View.OnClickListener { v ->
            try {
                view.isClickable = false
                val file = File(mLista[position])
                if (file.isFile)
                    UtilidadesValidity.showImageGallery(v.context, file.absolutePath, view)
            } catch (err: Exception) {
                view.isClickable = true
                LogTrace.logCatch(v.context, v.context.javaClass, err, true)
            }
        }
    }

    override fun getItemCount(): Int {
        return mLista.size
    }
}