package com.spotpromo.spot_validity.components

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.model.Produto
import com.spotpromo.spot_validity.util.LogTrace
import java.lang.Exception
import java.lang.StringBuilder

class AdapterProdutoMarca
constructor(private var context: Context, private var lista: ArrayList<Produto>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder: ViewHolder
        var mView = convertView
        try {
            if (mView == null) {
                mView = LayoutInflater.from(context).inflate(R.layout.adapter_generic_spot_validity, parent, false)
                holder = ViewHolder(mView)
                mView.tag = holder
            } else {
                holder = mView.tag as ViewHolder
            }

            var sbHtml = StringBuilder()
            sbHtml.append(String.format("%s", lista[position].desMarca.toString()))

            holder.nome.text = Html.fromHtml(sbHtml.toString())
        } catch (err: Exception) {
            LogTrace.logCatch(context, context.javaClass, err, true)
        }

        return mView!!
    }


    override fun getItem(position: Int): Any {
        return lista[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return lista.size
    }

    internal inner class ViewHolder(v: View) {
        var nome: TextView

        init {
            this.nome = v.findViewById(R.id.nome) as TextView
        }
    }

}