package com.spotpromo.spot_validity.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.item_validade_spot_validity.view.*
import kotlinx.android.synthetic.main.recycle_generic_valdity.view.*

class RecyclerListaProdutosAVencerSpotValidity  (val items: ArrayList<ValidadeSpotValidity>, val activity: AppCompatActivity, val viewmodel: ListaValidadeViewModel) : RecyclerView.Adapter<RecyclerListaProdutosAVencerSpotValidity.ViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_generic_valdity, parent, false)

        return ViewModel(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val item = items[position]
        with(holder.itemView) {

            var sbHtml = StringBuilder()

            val color: String =
                when (item.status) {
                    0 -> context.resources.getString(R.string.chamado_vermelho)
                    1 -> context.resources.getString(R.string.chamado_amarelo)
                    else ->
                        context.resources.getString(R.string.chamado_verde)
                }

            when (item.status) {
                0 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.vermelho))
                1 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.amarelo))
                2 -> ln_status.setBackgroundColor(context.resources.getColor(R.color.verde))
            }

            when(item.nivel) {
                0 -> sbHtml.append(
                    String.format(
                        "<br>Promotor: <b><font color='$color'>%s</font><b/></br>",
                        item.desPessoa
                    )
                )

                1 -> sbHtml.append(
                    String.format(
                        "<br>Loja: <b><font color='$color'>%s</font><b/></br>",
                        item.desLoja
                    )
                )

                2 -> {
                    sbHtml.append(
                        String.format(
                            "<br>Categoria: <b><font color='$color'>%s</font><b/></br>",
                            item.desCategoria
                        )
                    )
                    sbHtml.append(
                        String.format(
                            "<br>Marca: <b><font color='$color'>%s</font><b/></br>",
                            item.desMarca
                        )
                    )
                    sbHtml.append(
                        String.format(
                            "<br>Sku: <b><font color='$color'>%s</font><b/></br>",
                            item.desSku
                        )
                    )
                    sbHtml.append(
                        String.format(
                            "<br>Lote: <b><font color='$color'>%s</font><b/></br>",
                            item.lote
                        )
                    )
                    sbHtml.append(
                        String.format(
                            "<br>Data de Vencimento: <b><font color='$color'>%s</font><b/></br>",
                            item.data_validade
                        )
                    )
                }
            }


            nome.text = HtmlCompat.fromHtml(sbHtml.toString(), HtmlCompat.FROM_HTML_MODE_COMPACT)
            mrow.setOnClickListener {
                val onSelectItem = activity as onSelectItem
                when(item.nivel) {
                    0 -> onSelectItem.onSelect(item.codPessoa!!, 0, viewmodel)
                    1 -> onSelectItem.onSelect(item.codPessoa!!, item.codLoja!!, viewmodel)
                }
            }
        }
    }

    interface onSelectItem {
        fun onSelect(codPessoa: Int, codLoja: Int, viewmodel: ListaValidadeViewModel)
    }

    class ViewModel(view: View) : RecyclerView.ViewHolder(view)
}