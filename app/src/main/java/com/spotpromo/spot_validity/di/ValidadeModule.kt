package com.spotpromo.spot_validity.di

import com.spotpromo.spot_validity.viewmodel.ColetaValidadeViewModel
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object ValidadeModule {
    private val modules = module {
        viewModel { ColetaValidadeViewModel()  }
        viewModel { ListaValidadeViewModel() }

    }

    fun loadModules() {
        loadKoinModules(
            listOf(
                modules
            )
        )
    }
}