package com.spotpromo.spot_validity.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.SqliteDataBaseHelper
import com.spotpromo.spot_validity.util.dao.ColetaValidadeSpotValidityDAOHelper

class ColetaValidadeViewModel : ViewModel(){
    fun insertColeta(coletaValidadeSpotValidity: ValidadeSpotValidity, context: Context) {
        val daoColetaValidity = ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context))
        coletaValidadeSpotValidity.id =
            daoColetaValidity.insert(coletaValidadeSpotValidity)

        coletaValidadeSpotValidity.lista_foto?.forEach {
            daoColetaValidity.insertFoto(it, coletaValidadeSpotValidity.id!!)
        }
    }
}