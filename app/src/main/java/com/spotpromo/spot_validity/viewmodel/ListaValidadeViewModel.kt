package com.spotpromo.spot_validity.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.SqliteDataBaseHelper
import com.spotpromo.spot_validity.util.dao.ColetaValidadeSpotValidityDAOHelper

class ListaValidadeViewModel : ViewModel(){
    fun loadList(codPesquisa: Int, context: Context): ArrayList<ValidadeSpotValidity> {
        return ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context)).selectValidade(
            codPesquisa
        )
    }

    fun loadPessoalist(context: Context, nivel: Int): ArrayList<ValidadeSpotValidity> {
        return ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context)).selectPessoas(nivel)
    }

    fun loadLojalist(context: Context, codPessoa: Int, nivel: Int): ArrayList<ValidadeSpotValidity> {
        return ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context)).selectLojas(nivel, codPessoa)
    }

    fun loadProdutolist(context: Context, codPessoa: Int, codLoja: Int, nivel: Int): ArrayList<ValidadeSpotValidity> {
        return ColetaValidadeSpotValidityDAOHelper(SqliteDataBaseHelper.openDB(context)).selectProdutos(nivel, codPessoa, codLoja)
    }
}