package com.spotpromo.spot_validity.config

import com.spotpromo.spot_validity.view.lista.ActivityListaValidade
 public object SpotValidityConfig {
     var tamanho: String = ""
         get() = field
         set(value) {
             field = value
         }
     var color: String = ""
         get() = field
         set(value) {
             field = value
         }
     var principal_path: String = ""
         get() = field
         set(value) {
             field = value
         }
     var app_name = ""
         get() = field
         set(value) {
             field = value
         }
     var classTo : Class<*> = ActivityListaValidade::class.java
         get() = field
         set(value) {
             field = value
         }
     var db_name = ""
         get() = field
         set(value) {
             field = value
         }
 }