package com.spotpromo.spot_validity.customview.lista

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.RecyclerListaProdutosAVencerSpotValidity
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.layout_recycler_spot_validity.view.*

class ListaProdutosAVencerView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs), RecyclerListaProdutosAVencerSpotValidity.onSelectItem {

    private lateinit var adapter: RecyclerListaProdutosAVencerSpotValidity
    private lateinit var activity: AppCompatActivity
    private var nivel = 0
    private var codPessoa = 0
    private var codLoja = 0

    init {
        View.inflate(context, R.layout.layout_recycler_spot_validity, this)
    }

    fun loadComponents(activity: AppCompatActivity, viewmodel: ListaValidadeViewModel) {
        mprogress.visibility = View.VISIBLE
        txtnotfound.visibility = View.GONE
        this.activity = activity

        val lista = refreshList(viewmodel)
        if(lista.isNullOrEmpty()) {
            mprogress.visibility = View.GONE
            txtnotfound.visibility = View.VISIBLE
        } else {
            mprogress.visibility = View.GONE
            txtnotfound.visibility = View.GONE
        }

        adapter =
            RecyclerListaProdutosAVencerSpotValidity(lista, activity = activity, viewmodel)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        rvListaSpotValidity.layoutManager = linearLayoutManager
        rvListaSpotValidity.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    fun getNivel() = nivel
    fun setNivel(valor : Int) {
        this.nivel = valor
    }

    fun refreshList(viewmodel: ListaValidadeViewModel) : ArrayList<ValidadeSpotValidity> {
        return when(nivel){
            0 -> {viewmodel.loadPessoalist(context, nivel)}
            1 -> {viewmodel.loadLojalist(context, codPessoa, nivel)}
            else -> {viewmodel.loadProdutolist(context, codPessoa, codLoja,  nivel)}
        }
    }

    override fun onSelect(codPessoa: Int, codLoja: Int, viewmodel: ListaValidadeViewModel) {
        nivel++
        this.codLoja = codLoja
        this.codPessoa = codPessoa
        loadComponents(activity, viewmodel)
    }
}