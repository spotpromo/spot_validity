package com.spotpromo.spot_validity.customview.selectsku

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.util.Util
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.AdapterProdutoCategoria
import com.spotpromo.spot_validity.components.AdapterProdutoMarca
import com.spotpromo.spot_validity.components.AdapterProdutoSku
import com.spotpromo.spot_validity.components.RecyclerListaValidadeSpotValidity
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.Produto
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.SqliteDataBaseHelper
import com.spotpromo.spot_validity.util.UtilidadesValidity
import com.spotpromo.spot_validity.util.dao.ProdutoDAOHelper
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.layout_spinner_sku_spot_validity.view.*

class SelectSkuView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs), AdapterView.OnItemSelectedListener {
    private val listaCategorias = ArrayList<Produto>()
    private val listaMarcas = ArrayList<Produto>()
    private val listaSkus = ArrayList<Produto>()
    private var mColetaValidade: ValidadeSpotValidity? = null
    private var retornarMarca = false
    private var retornarSku= false
    private lateinit var daoProduto : ProdutoDAOHelper

    init {
        View.inflate(context, R.layout.layout_spinner_sku_spot_validity, this)
    }

    fun loadComponents(activity: AppCompatActivity) {
        daoProduto = ProdutoDAOHelper(SqliteDataBaseHelper.openDB(activity))

        listaCategorias.addAll(daoProduto.selectCategoriasSpotValidity())
        listaMarcas.add(daoProduto.selecioneMarca())
        listaSkus.add(daoProduto.selecioneSku())

        spn_categoria.adapter = AdapterProdutoCategoria(activity, listaCategorias)
        spn_marca.adapter = AdapterProdutoMarca(activity, listaMarcas)
        spn_sku.adapter = AdapterProdutoSku(activity, listaSkus)

        spn_categoria.onItemSelectedListener = this
        spn_marca.onItemSelectedListener = this
    }

    fun getCategoria() = spn_categoria.selectedItem as Produto
    fun getMarca() = spn_marca.selectedItem as Produto
    fun getSku() = spn_sku.selectedItem as Produto

    fun validateSpinners(activity: AppCompatActivity) : Boolean {
        return !(!UtilidadesValidity.validaSpinnerCategoria(spn_categoria, activity) ||
                !UtilidadesValidity.validaSpinnerMarca(spn_marca, activity) ||
                !UtilidadesValidity.validaSpinnerSku(spn_sku, activity))
    }

    fun setUpRetorno(mColetaValidade: ValidadeSpotValidity) {
        this.mColetaValidade = mColetaValidade

        for(i in listaCategorias.indices)
            if(mColetaValidade.codCategoria == listaCategorias[i].codCategoria)
                spn_categoria.setSelection(i)

        retornarMarca = true
        retornarSku = true
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when(parent) {
            spn_categoria -> {
                val categoria = spn_categoria.selectedItem as Produto
                listaMarcas.clear()
                if (categoria.codCategoria!! > 0)
                    listaMarcas.addAll(daoProduto.selectMarcasSpotValidity(categoria.codCategoria!!))
                else
                    listaMarcas.add(daoProduto.selecioneMarca())

                if(retornarMarca) {
                    if (mColetaValidade != null) {
                        for (i in listaMarcas.indices)
                            if (mColetaValidade!!.codMarca == listaMarcas[i].codMarca)
                                spn_marca.setSelection(i)
                    }
                }

                retornarMarca = false
            }

            spn_marca -> {
                val categoria = spn_categoria.selectedItem as Produto
                val marca = spn_marca.selectedItem as Produto
                listaSkus.clear()

                if (marca.codMarca!! > 0)
                    listaSkus.addAll(
                        daoProduto.selectSkuSpotValidity(
                            codCategoria = categoria.codCategoria!!,
                            codMarca = marca.codMarca!!
                        )
                    )
                else
                    listaSkus.add(daoProduto.selecioneSku())

                if(retornarSku) {
                    if (mColetaValidade != null) {
                        for (i in listaSkus.indices)
                            if (mColetaValidade!!.codSku == listaSkus[i].codSku)
                                spn_sku.setSelection(i)
                    }
                }

                retornarSku = false

            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
}