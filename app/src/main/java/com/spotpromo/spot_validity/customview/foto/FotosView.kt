package com.spotpromo.spot_validity.customview.foto

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.RecycleValidityFotos
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.Alerta
import com.spotpromo.spot_validity.util.LogTrace
import com.spotpromo.spot_validity.util.UtilidadesValidity
import kotlinx.android.synthetic.main.activity_coleta_validade_spot_validity.*
import kotlinx.android.synthetic.main.layout_lista_fotos_spot_validity.view.*
import java.io.File
import java.lang.Exception

class FotosView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {

    private lateinit var activity: AppCompatActivity
    private lateinit var mRecycle : RecycleValidityFotos
    private val listaFotos = ArrayList<String>()
    private var codRoteiro = 0

    init {
        View.inflate(context, R.layout.layout_lista_fotos_spot_validity, this)
    }

    fun loadComponents(activity: AppCompatActivity, codRoteiro: Int) {
        this.codRoteiro = codRoteiro
        this.activity = activity

        recycle.setHasFixedSize(true)
        val mLayoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL, false
        )

        recycle.layoutManager = mLayoutManager
        recycle.addItemDecoration(
            DividerItemDecoration(
                activity,
                DividerItemDecoration.HORIZONTAL
            )
        )
        mRecycle = RecycleValidityFotos(activity, listaFotos)
        recycle.adapter = mRecycle

        fabAddFotoValidity.backgroundTintList = ColorStateList.valueOf(Color.parseColor(SpotValidityConfig.color))
        fabAddFotoValidity.setOnClickListener {
            UtilidadesValidity.abrirCameraNova(activity, 1)
        }
    }


    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        try {
            /** FOTO RECYCLE **/
            if (resultCode == RESULT_OK && requestCode == 1) {
                val fotoAntiga = data!!.extras!!.getString("arquivo")!!
                val file = File(fotoAntiga)

                if (file.isFile) {

                    val mFotoNova = UtilidadesValidity.retornaCaminhoFotoURINew(
                        Uri.fromFile(File(fotoAntiga)),
                        codRoteiro,
                        "Foto_Validade",
                        activity
                    )!!

                    file.delete()

                    listaFotos.add(mFotoNova)
                    refreshRecycler()
                } else {
                    Alerta.show(
                        activity,
                        resources.getString(R.string.msg_atencao),
                        resources.getString(R.string.msg_erro_foto_capturar),
                        false
                    )
                }

            }


        } catch (err: Exception) {
            LogTrace.logCatch(activity, this.javaClass, err, true)
        }
    }

    fun validaFotos(activityColetaValidadeSpotValidity: AppCompatActivity): Boolean {
        return if(listaFotos.size == 0) {
            Alerta.show(activityColetaValidadeSpotValidity, context.getString(R.string.msg_atencao),
                context.getString(R.string.coleta_foto_validade), false)
            false
        }else
            true
    }

    fun getListaFotos(): ArrayList<String> {
        return listaFotos
    }

    fun setUpRetorno(coletaSpotValidity: ValidadeSpotValidity) {
        listaFotos.addAll(coletaSpotValidity.lista_foto!!)
        refreshRecycler()
    }

    private fun refreshRecycler() {
        mRecycle.notifyDataSetChanged()
        mRecycle.notifyItemInserted(listaFotos.size)
        recycle.smoothScrollToPosition(listaFotos.size)
    }
}