package com.spotpromo.spot_validity.customview.lista

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.RecyclerListaValidadeSpotValidity
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.util.SendIntent
import com.spotpromo.spot_validity.view.coleta.ActivityColetaValidadeSpotValidity
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.activity_coleta_validade_spot_validity.*
import kotlinx.android.synthetic.main.layout_recycler_buttom_spot_validity.view.*

class ListaValidadeView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {

    private lateinit var adapter: RecyclerListaValidadeSpotValidity

    init {
        View.inflate(context, R.layout.layout_recycler_buttom_spot_validity, this)
    }

    fun loadComponents(activity: AppCompatActivity, viewmodel: ListaValidadeViewModel, codPesquisa: Int, codRoteiro: Int) {
        mprogress.visibility = View.VISIBLE
        txtnotfound.visibility = View.GONE

        val lista = viewmodel.loadList(codPesquisa, context)
        if(lista.isNullOrEmpty()) {
            mprogress.visibility = View.GONE
            txtnotfound.visibility = View.VISIBLE
        } else {
            mprogress.visibility = View.GONE
            txtnotfound.visibility = View.GONE
        }

        adapter =
            RecyclerListaValidadeSpotValidity(lista, activity = activity, codRoteiro)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)

        rvListaSpotValidity.layoutManager = linearLayoutManager
        rvListaSpotValidity.adapter = adapter
        adapter.notifyDataSetChanged()

        fbAddSpotValidity.backgroundTintList = ColorStateList.valueOf(Color.parseColor(SpotValidityConfig.color))
        fbAddSpotValidity.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("codRoteiro", codRoteiro)
            bundle.putInt("codPesquisa", codPesquisa)

            SendIntent.with()
                .mClassFrom(activity)
                .mBundle(bundle)
                .mType(R.integer.slide_from_right)
                .mClassTo(ActivityColetaValidadeSpotValidity::class.java)
                .go()
        }
    }

    fun notifyItem(position: Int) {
        adapter.notifyItemChanged(position)
    }
}