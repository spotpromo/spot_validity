package com.spotpromo.spot_validity.customview.toolbar

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.config.SpotValidityConfig
import com.spotpromo.spot_validity.util.SendIntent
import kotlinx.android.synthetic.main.layout_toolbar_spot_validity.view.*


class ToolbarView (context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {

    init {
        View.inflate(context, R.layout.layout_toolbar_spot_validity, this)
    }

    fun loadToolbar(title: String, subTitle: String, classfrom: Activity, classTo: Class<*>, codPesquisa: Int, codRoteiro: Int) {
        toolbarSpotValidity.background = ColorDrawable(Color.parseColor(SpotValidityConfig.color))
            ColorStateList.valueOf(Color.parseColor(SpotValidityConfig.color))
        ivVoltarValidity.background = ColorDrawable(Color.parseColor(SpotValidityConfig.color))
        ColorStateList.valueOf(Color.parseColor(SpotValidityConfig.color))
        toolbarSpotValidity.title = title
        toolbarSpotValidity.subtitle = subTitle
        setUpBackPressed(classfrom, classTo, codPesquisa, codRoteiro)

        val window: Window = classfrom.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.parseColor(SpotValidityConfig.color)

    }

    private fun setUpBackPressed(classfrom: Activity, classTo: Class<*>, codPesquisa: Int, codRoteiro: Int) {
        ivVoltarValidity.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("codRoteiro", codRoteiro)
            bundle.putInt("codPesquisa", codPesquisa)
            SendIntent.with()
                .mClassFrom(classfrom)
                .mBundle(bundle)
                .mClassTo(classTo)
                .mType(R.integer.slide_from_left)
                .go()
        }
    }
}