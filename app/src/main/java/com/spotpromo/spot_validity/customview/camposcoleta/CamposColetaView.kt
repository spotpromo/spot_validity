package com.spotpromo.spot_validity.customview.camposcoleta

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.spotpromo.spot_validity.R
import com.spotpromo.spot_validity.components.RecyclerListaValidadeSpotValidity
import com.spotpromo.spot_validity.model.ColetaSpotValidity
import com.spotpromo.spot_validity.model.ValidadeSpotValidity
import com.spotpromo.spot_validity.util.DataDialog
import com.spotpromo.spot_validity.util.UtilidadesValidity
import com.spotpromo.spot_validity.viewmodel.ListaValidadeViewModel
import kotlinx.android.synthetic.main.layout_validity_fields_spot_validity.view.*

class CamposColetaView(context: Context, attrs: AttributeSet?) :
    ConstraintLayout(context, attrs) {

    private lateinit var adapter: RecyclerListaValidadeSpotValidity

    init {
        View.inflate(context, R.layout.layout_validity_fields_spot_validity, this)
    }

    fun loadComponents(activity: AppCompatActivity) {
        edt_data_vencimento.setOnClickListener(DataDialog(edt_data_vencimento, activity))
    }

    fun validateFields(activity: AppCompatActivity) : Boolean {
        return !(!UtilidadesValidity.validaEditTextInteiro(edt_quantidade, activity, true) ||
                !UtilidadesValidity.validaEditText(edt_lote, activity) ||
                !UtilidadesValidity.validaEditText(edt_data_vencimento, activity) ||
                !UtilidadesValidity.validaEditText(edt_observacao, activity))
    }

    fun getQtd(): Int = edt_quantidade.text.toString().toInt()
    fun getLote() : String = edt_lote.text.toString()
    fun getDataVencimento() : String = edt_data_vencimento.text.toString()
    fun getObservacoes() : String = edt_observacao.text.toString()

    fun setUpRetorno(mColetaValidade: ValidadeSpotValidity) {
        edt_quantidade.setText(mColetaValidade.quantidade.toString())
        edt_lote.setText(mColetaValidade.lote)
        edt_data_vencimento.setText(mColetaValidade.data_validade)
        edt_observacao.setText(mColetaValidade.observacoes)
    }
}